CREATE DATABASE  IF NOT EXISTS `ejemplo1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ejemplo1`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: ejemplo1
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bicicletas`
--

DROP TABLE IF EXISTS `bicicletas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bicicletas` (
  `bicicletas_fabricante` char(20) NOT NULL,
  `bicicletas_precio_unitario` int DEFAULT NULL,
  `bicicletas_annio` int DEFAULT NULL,
  PRIMARY KEY (`bicicletas_fabricante`),
  CONSTRAINT `bicicletas_ibfk_1` FOREIGN KEY (`bicicletas_fabricante`) REFERENCES `fabricantes` (`marcas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bicicletas`
--

LOCK TABLES `bicicletas` WRITE;
/*!40000 ALTER TABLE `bicicletas` DISABLE KEYS */;
INSERT INTO `bicicletas` VALUES ('Bmc',1950000,1018),('Cannondale',1200000,2017),('Fuji',950000,2021),('Trek',1450000,2019),('Yeti',2000000,2020);
/*!40000 ALTER TABLE `bicicletas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `cliente_alias` char(20) NOT NULL,
  `cliente_nombres` char(20) DEFAULT NULL,
  `cliente_apellidos` char(20) DEFAULT NULL,
  `cliente_email` char(45) DEFAULT NULL,
  `cliente_celular` char(20) DEFAULT NULL,
  `cliente_contrasena` int DEFAULT NULL,
  `cliente_fecha_de_nacimiento` date DEFAULT NULL,
  PRIMARY KEY (`cliente_alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES ('diva','Ana','Diaz','ana_diaz@gmail.com','3111234567',12345678,'2000-11-22'),('dreamer','Luis','Rojas','luis_rojas@gmail.com','3101234567',12345678,'2000-10-23'),('green','Jorge','Rodriguez','jorge_rodriguez@gmail.com','3211234567',12345678,'2000-10-28'),('lucky','Pedro','Perez','pedro_perez@gmail.com','3131234567',12345678,'2000-12-24'),('malopez','Maria','Lopez','maria_lopez@gmail.com','3121234567',12345678,'2000-12-29'),('neon','Nelson','Ruiz','nelson_ruiz@gmail.com','3151234567',12345678,'2000-12-19'),('ninja','Andres','Cruz','andres_cruz@gmail.com','3115678432',12345678,'2000-09-21'),('prueba','prueba1','prueba1','prueba1','11111',11111,'2000-11-11'),('prueba2','prueba2','prueba2','prueba2','11111',11111,'2000-01-11'),('prueba3','prueba3','prueba3','prueba3','111111',111111,'2000-11-11'),('rose','Claudia','Mendez','claudia_mendez@gmail.com','3181234567',12345678,'2000-11-26');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabricantes`
--

DROP TABLE IF EXISTS `fabricantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fabricantes` (
  `marcas` char(20) NOT NULL,
  PRIMARY KEY (`marcas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabricantes`
--

LOCK TABLES `fabricantes` WRITE;
/*!40000 ALTER TABLE `fabricantes` DISABLE KEYS */;
INSERT INTO `fabricantes` VALUES ('Aima'),('Atom Electric'),('Be Electric'),('Bmc'),('Cannondale'),('Fuji'),('Lucky Lion'),('Mec de Colombia'),('Starker'),('Trek'),('Yeti');
/*!40000 ALTER TABLE `fabricantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intenciones_compra`
--

DROP TABLE IF EXISTS `intenciones_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `intenciones_compra` (
  `inte_alias` char(20) DEFAULT NULL,
  `inte_marca` char(20) DEFAULT NULL,
  `inte_fecha` char(70) DEFAULT NULL,
  KEY `inte_alias` (`inte_alias`),
  KEY `inte_marca` (`inte_marca`),
  CONSTRAINT `intenciones_compra_ibfk_1` FOREIGN KEY (`inte_alias`) REFERENCES `cliente` (`cliente_alias`),
  CONSTRAINT `intenciones_compra_ibfk_2` FOREIGN KEY (`inte_marca`) REFERENCES `fabricantes` (`marcas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intenciones_compra`
--

LOCK TABLES `intenciones_compra` WRITE;
/*!40000 ALTER TABLE `intenciones_compra` DISABLE KEYS */;
INSERT INTO `intenciones_compra` VALUES ('lucky','Cannondale','2017-10-25 20:00:00'),('lucky','Trek','2019-03-15 18:30:00'),('lucky','Starker','2019-05-20 20:30:00'),('malopez','Cannondale','2018-05-20 20:30:00'),('malopez','Starker','2020-01-20 20:30:00'),('diva','Yeti','2019-05-20 20:30:00'),('diva','Fuji','2018-06-22 21:30:00'),('diva','Lucky Lion','2020-03-17 15:30:20'),('dreamer','Lucky Lion','2020-03-17 15:30:20'),('dreamer','Be Electric','2020-04-10 18:30:20'),('ninja','Aima','2020-02-17 20:30:20'),('ninja','Starker','2020-02-20 16:30:20'),('ninja','Mec de Colombia','2020-03-27 18:30:20'),('rose','Atom Electric','2020-03-20 21:30:20'),('green','Yeti','2020-01-10 17:30:20'),('green','Bmc','2020-03-17 18:30:20');
/*!40000 ALTER TABLE `intenciones_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motocicletas_electricas`
--

DROP TABLE IF EXISTS `motocicletas_electricas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `motocicletas_electricas` (
  `motoelec_fabricante` char(20) NOT NULL,
  `motoelec_precio_unitario` int DEFAULT NULL,
  `motoelec_autonomia` int DEFAULT NULL,
  `motoelec_proveedor_del_motor` char(45) DEFAULT NULL,
  `motoelec_motor_prov_id` int DEFAULT NULL,
  PRIMARY KEY (`motoelec_fabricante`),
  KEY `motoelec_motor_prov_id` (`motoelec_motor_prov_id`),
  CONSTRAINT `motocicletas_electricas_ibfk_1` FOREIGN KEY (`motoelec_fabricante`) REFERENCES `fabricantes` (`marcas`),
  CONSTRAINT `motocicletas_electricas_ibfk_2` FOREIGN KEY (`motoelec_motor_prov_id`) REFERENCES `proveedor` (`prov_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motocicletas_electricas`
--

LOCK TABLES `motocicletas_electricas` WRITE;
/*!40000 ALTER TABLE `motocicletas_electricas` DISABLE KEYS */;
INSERT INTO `motocicletas_electricas` VALUES ('Aima',8000000,36,'Bosch',103),('Atom Electric',4500000,12,'General Electric',105),('Be Electric',4600000,26,'Auteco',101),('Lucky Lion',5600000,14,'Hitachi',102),('Mec de Colombia',5900000,20,'Teco',104),('Starker',4200000,18,'Auteco',101);
/*!40000 ALTER TABLE `motocicletas_electricas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proveedor` (
  `prov_id` int NOT NULL,
  `prov_nombre` char(20) DEFAULT NULL,
  `prov_direccion` char(45) DEFAULT NULL,
  `prov_telefono` char(30) DEFAULT NULL,
  PRIMARY KEY (`prov_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (101,'Auteco','calle 7 No. 45-17','05713224459'),(102,'Hitachi','calle 19 No. 108-26','05714223344'),(103,'Bosh','carrera 68 No. 26-45','05715678798'),(104,'Teco','calle 77 No. 68-33','05712213243'),(105,'General Electric','calle 29 No. 26-12','05717239919');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'ejemplo1'
--

--
-- Dumping routines for database 'ejemplo1'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-05 14:20:26
